import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "io.palutz",
      scalaVersion := "2.11.8",
      version      := "0.1.0"
    )),
    name := "Skimlinks",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "net.databinder.dispatch" %% "dispatch-core" % "0.11.2",
      "joda-time" % "joda-time" % "2.9.7",
      "org.joda" % "joda-convert" % "1.8",
//      "io.circe" %% "circe-core" % "0.6.1",
//      "io.circe" %% "circe-generic" % "0.6.1",
//      "io.circe" %% "circe-parser" % "0.6.1"
      "io.argonaut" %% "argonaut" % "6.1"
    )
  )
