//package io.palutz
//
//import java.security.MessageDigest
//
//import akka.actor.{Actor, ActorLogging}
//import akka.http.scaladsl.Http
//import akka.http.scaladsl.model._
//import akka.stream.ActorMaterializer
//import akka.stream.ActorMaterializerSettings
//import akka.util.ByteString
//
//import scala.util.{Failure, Success, Try}
//
//class MarvelWebClient extends Actor with ActorLogging {
//
//  import akka.pattern.pipe
//  import context.dispatcher
//
//
//  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
//
//  val http = Http(context.system)
//
//  val publicKey = "e94b5fe75caf4563f8dfb537f48c7ec0"
//  val privateKey = "1e530b27c8058036fdd92303714ead1473667556"
//
//  //Server-side applications must pass two parameters in addition to the apikey parameter:
//  //
//  //ts - a timestamp (or other long string which can change on a request-by-request basis)
//  //hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)
//  //For example, a user with a public key of "1234" and a private key of "abcd" could construct a valid call as follows:
//  // http://gateway.marvel.com/v1/comics?ts=1&apikey=1234&hash=ffd275c5130566a2916217b101f26150
//  //
//  // (the hash value is the md5 digest of 1abcd1234)
//
//  //https://gateway.marvel.com:443/v1/public/characters?apikey=e94b5fe75caf4563f8dfb537f48c7ec0
//
//  private def hashIt(strData: String, algo: String): Try[String] = {
//    return Try(MessageDigest.getInstance(algo).digest(strData.getBytes).map("%02x".format(_)).mkString)
//  }
//
//  override def preStart() = {
//    val salt = DateTime.now.clicks
//    val strHash = s"$salt$privateKey$publicKey"
//    val sUrl = hashIt(strHash, "MD5") match {
//      case Success(h) => {
//        var url = s"https://gateway.marvel.com:443/v1/public/characters?ts=$salt&apiKey=$publicKey&hash=$h"
//        http.singleRequest(HttpRequest(uri = url)).pipeTo(self)
//      }
//      case Failure(_) => ""
//    }
//  }
//
//  def receive = {
//    case HttpResponse(StatusCodes.OK, headers, entity, _) =>
//      log.info("Got response, body: " + entity.dataBytes.runFold(ByteString(""))(_ ++ _))
//    case HttpResponse(code, _, _, _) =>
//      log.info("Request failed, response code: " + code)
//  }
//
//}
