package io.palutz


//class PopularStore(size: Int) {
//  def addElem
//}
//
//object PopularStore {
//  def apply(size: Int) = {
//    new PopularStore(size)
//  }
//}
abstract class PopularStore(name: String, aVal : Int) {}

case class MostPopular(name: String, av: Int) extends PopularStore(name = name, aVal = av)

object Comparer {
  def compareAndAdd(character: (String, Int), topX: Array[(String, Int)], maxElem: Int): Array[(String, Int)] = {
    if (topX.length == maxElem) {
      if (character._2 > topX(maxElem - 1)._2) {
        Array.concat(
          topX filter (character._2 < _._2),
          topX filter (character._2 == _._2),
          Array(("z", character._2)),
          topX filter (character._2 > _._2)).take(maxElem)
      } else
        topX
    } else
      (topX :+ character).sortWith(_._2 > _._2)
  }
}