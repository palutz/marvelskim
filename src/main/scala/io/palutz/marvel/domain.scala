package io.palutz.marvel

// import org.joda.time._

case class CharacterDataWrapper(code: Int, status: String, copyright: String, attributionText: String, attributionHTML: String, data: CharacterDataContainer, etag: String)

case class CharacterDataContainer(offset: Int, limit: Int, total: Int, count: Int, result: List[MarvelCharacter])

// case class Character(id: Option[Int], name: Option[String], description: Option[String], modified: Option[DateTime], resourceURI: Option[String], urls: Option[List[CharacterUrl]], thumbnail: Option[List[Thumbnail]],
case class MarvelCharacter(id: Int, name: String, description: String, modified: String, thumbnail: Thumbnail, resourceURI: String,
                           comics: InfoListItem,
                           series: InfoListItem,
                           stories: InfoListItem,
                           events: InfoListItem,
                           urls: List[CharacterUrl])

case class CharacterUrl(utype: String, url: String)
case class Thumbnail(path: String, extension: String)

case class InfoListItem(available: Int, returned: Int, collectionURI: String, items: List[SummaryItem])
case class SummaryItem(resourceURI: String, name: String, stype: String)
