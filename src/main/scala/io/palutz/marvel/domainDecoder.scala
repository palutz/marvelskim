package io.palutz.marvel

import argonaut._, Argonaut._

//import io.circe.Decoder.Result
//import io.circe._
//import io.circe.generic.semiauto._
//import org.joda.time._

// Define all the decoders for circe (Json lib)
//object domainDecoder {
//  implicit val wrapperDecoder: Decoder[CharacterDataWrapper] = deriveDecoder[CharacterDataWrapper]
//  implicit val containerDecoder: Decoder[CharacterDataContainer] = deriveDecoder[CharacterDataContainer]
//  implicit val characterDecoder: Decoder[MarvelCharacter] = deriveDecoder[MarvelCharacter]
//  implicit val urlDecoder: Decoder[CharacterUrl] = deriveDecoder[CharacterUrl]
//  implicit val thumbDecore: Decoder[Thumbnail] = deriveDecoder[Thumbnail]
//  implicit val ilItemDecoder: Decoder[InfoListItem] = deriveDecoder[InfoListItem]
//  //implicit val summaryDecoder: Decoder[SummaryItem] = deriveDecoder[SummaryItem]
//
//  // trying to decode/encode the datetime type for adn from json
//  implicit val TheForceAwakens : Encoder[DateTime] with Decoder[DateTime] = new Encoder[DateTime] with Decoder[DateTime] {
//    override def apply(d: DateTime): Json = Encoder.encodeString.apply(d.toString)
//    override def apply(c: HCursor): Result[DateTime] = Decoder.decodeLong.map(s => new DateTime(s)).apply(c)
//  }

object domainDecoder {
  implicit def CharacterDataWrapperJson: CodecJson[CharacterDataWrapper] =
    casecodec7(CharacterDataWrapper.apply, CharacterDataWrapper.unapply)("code", "status", "copyright", "attributionText", "attributionHTML", "data", "etag")

  implicit def CharacterDataContainerJson : CodecJson[CharacterDataContainer] =
    casecodec5(CharacterDataContainer.apply, CharacterDataContainer.unapply)("offset", "limit", "total", "count", "result")

  implicit def MarvelCharacterJson : CodecJson[MarvelCharacter] =
    casecodec11(MarvelCharacter.apply, MarvelCharacter.unapply)("id","name","description","modified","thumbnail","resourceURI",
                             "comics","series","stories","events","urls")


  implicit def CharacterUrlEncodeJson : EncodeJson[CharacterUrl] =
    jencode2L((c: CharacterUrl) => (c.url, c.utype))("url","type")

  implicit def CharacterUrlDecodeJson : DecodeJson[CharacterUrl] =
    DecodeJson(c => for {
      t <- (c --\ "type").as[String]
      u <- (c --\ "url").as[String]
    } yield CharacterUrl(t, u))


  implicit def ThumbnaillJson : CodecJson[Thumbnail] =
    casecodec2(Thumbnail.apply, Thumbnail.unapply)("path", "extension")


  implicit def InfoListItemJson : CodecJson[InfoListItem] =
    casecodec4(InfoListItem.apply, InfoListItem.unapply)("available","returned","collectionURI","items")


  implicit def SummaryItemEncodeJson : EncodeJson[SummaryItem] =
    jencode3L((s: SummaryItem) => (s.name, s.resourceURI, s.stype))("resourceURI","name","type")

  implicit def SummaryItemDecodeJson : DecodeJson[SummaryItem] =
    DecodeJson(c => for {
      r <- (c --\ "resourceURI").as[String]
      n <- (c --\ "name").as[String]
      t <- (c --\ "type").as[String]
    } yield SummaryItem(r, n, t))
  //casecodec3(SummaryItem.apply, SummaryItem.unapply)("resourceURI","name","type")
}
