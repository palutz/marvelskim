package io.palutz

import org.joda.time._
import dispatch._
import Defaults._

import scala.concurrent.duration.Duration
import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import java.security.MessageDigest
import java.util.concurrent.TimeUnit._

import scala.annotation.tailrec

//import io.circe.parser.decode
//import io.circe._, io.circe.generic.semiauto._

import argonaut._, Argonaut._

import io.palutz.marvel._

import domainDecoder._


/*

val allWords: Map[String, Seq[String]] = 
  Source.fromFile(file)
        .getLines()
        .head
        .split(extractLabelResources)
        .groupBy { case (label, resource) => label }
        .mapValues(_.toSeq)

def extractLabelResources(line: String): Array[(String, String)] = {
    // ...
}

def search(word: String): Set[String] = allWords.getOrElse(word, Set.empty)

*/

object testHttp {
  private val publicKey = "e94b5fe75caf4563f8dfb537f48c7ec0"
  private val privateKey = "1e530b27c8058036fdd92303714ead1473667556"

  val basicUrl = "https://gateway.marvel.com:443/v1/public/characters?limit="

  private implicit def hashIt(strData: String, algo: String): Try[String] = {
    return Try(MessageDigest.getInstance(algo).digest(strData.getBytes).map("%02x".format(_)).mkString)
  }

  private def orderByName(count: Option[Int]) = {
    count match {
      case Some(x) => s"orderBy=name&limit=$x"
      case _ => s"orderBy=name"
    }
  }

  private def createUrl(qry: Option[String], algo: String, limit: Int = 99, offset: Int = 0)(implicit sign: (String,String) => Try[String]): Option[String] = {
    // val sUrl = s"https://gateway.marvel.com:443/v1/public/characters?ts=$salt&apikey=$publicKey&hash=$h"
    val salt = DateTime.now().getMillis
    val strHash = s"$salt$privateKey$publicKey"
    sign(strHash, algo) match {
      case Success(h) => Option(List(Option(basicUrl + limit), Option(s"offset=$offset"), qry, Option(s"ts=$salt&apikey=$publicKey&hash=$h")).
                              collect{ case Some(s) => s }.mkString("&"))   // using collect and partial function to select only the string available
      case _ => None
    }
  }


  // general function to create a request with Dispatch
  private def createRequest(qry: Option[String], limit: Int, offset: Int): Option[dispatch.Future[String]] =
    createUrl(qry, "MD5", limit, offset) match {
      case  Some(req) => Some(Http(url(req) OK as.String))
      case _ => None
    }

  // Show the result
  private def showReport(wrapper: CharacterDataWrapper): Unit = {
//    val maybeCh = wrapper.data match {
//      case Some(x) => {
//        println(s"x=$x")
//        Some(x.result.flatten)
//      }
//      case _ => None
//    }
    val maybeCh = Option(wrapper.data.result)
    //println(s"maybeCH=$maybeCh")
    maybeCh match {
      case Some(listCh) => for {
          oneCh <- listCh
        } yield println(oneCh.name)
      case _ => println("Ooopsss... Something went wrong!!!")
      }
  }

  private implicit def decoderCharactersAPI(urlReq: Option[Try[String]]): Either[String, Option[CharacterDataWrapper]] = {
    urlReq match {
      case Some(Success(x)) => {
        println(s"DecodeRes success=$x")
        Right(x.decodeOption[CharacterDataWrapper])
      }
      case Some(Failure(y)) => Left(s"Error processing the request: $y")
      case _ => Left("Generic Error")
    }
  }

  def queryAPIs(limit: Int = 99, offset: Int = 100)(implicit decoder: Option[Try[String]] => Either[String, Option[CharacterDataWrapper]]) : Option[List[CharacterDataWrapper]] = {
    // querying the provided url until we receive some data..
    @tailrec
    def inner(newLimit: Int, acc: List[CharacterDataWrapper]): List[CharacterDataWrapper] = {
      val topName = createRequest(Some(orderByName(None)), newLimit, offset)
      // waiting for the Future(s) to complete...
      val orderedByName = topName.flatMap(t => Await.ready(t, Duration(10, SECONDS)).value)
      //JSON parsing
      decoder(orderedByName) match {
        case Right(y) => {
          y match {
            case Some(cx) => inner((newLimit + offset), acc :+ cx)
            case _ => acc
          }
        }
        case _ => acc
      }
    }
    if(offset > 0)
      Some(inner(limit, List[CharacterDataWrapper]()))
    else
      None
  }

  // Main
  def main(args: Array[String]): Unit = {
    // val top10Name = createRequest(Some(orderByName(10)))
    // Console.println(res)

    for {
      qr <- queryAPIs()
      //
      // logic and search
      // _ <- showReport(x)
    } yield (true)


  }
}
